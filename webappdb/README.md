
Directorio de DB txt

## Base de Datos

La documentacion del sistema se divide en dos partes:

* Documentacion de desarrollo DB: [webappwebdb-README.md](webappwebdb-README.md)
* Documentacion ODBC saint: [odbc-README.md](odbc-README.md)
* Modulacion datos `mproductos` explicado: [oasis_productos.md](oasis_productos.md)

### Diseño

El diseño requiere uso de `mysql-workbench` para visualizar el archivo "mwb" .

* `webappwebdb.png` [webappwebdb.png](webappwebdb.png)
* `webappweb.mwb` [webappweb.mwb](webappweb.mwb)

### Diccionario datos

El diccionario de datos esta incluido en el scrip SQL, oasis no tiene aun:

* `webappwebdb.sql` [webappwebdb.sql](webappwebdb.sql)

### Script SQL

El script se puede ejecutar completo y creara la DB/schema pero no un usuario.

* `webappwebdb.sql` [webappwebdb.sql](webappwebdb.sql)


